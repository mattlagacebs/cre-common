package dbutil

import (
	"log"

	"github.com/dgraph-io/dgo"
	"github.com/dgraph-io/dgo/protos/api"
	"google.golang.org/grpc"
)

// DBClient is the db client that will be used for all workers.
type DBClient struct {
	conn   *grpc.ClientConn
	Client *dgo.Dgraph
}

// NewDBClient creates a new DBClient client that connects to the DB
func NewDBClient() *DBClient {
	dbc := &DBClient{}

	// Dial a gRPC connection. The address to dial to can be configured when
	// setting up the dgraph cluster.
	d, err := grpc.Dial("localhost:9080", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Failed to connect to Dgraph: %v", err)
		return nil
	}

	dbc.conn = d
	dbc.Client = dgo.NewDgraphClient(
		api.NewDgraphClient(d),
	)
	return dbc
}

// Close the connection to the DB
func (db *DBClient) Close() {
	defer db.conn.Close()
}
