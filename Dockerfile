FROM rabbitmq:3.7.8-alpine

WORKDIR /go/src/bitbucket.org/mattlagacebs/cre-common

COPY . .

RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh

CMD [ "/bin/bash", "/go/src/bitbucket.org/mattlagacebs/cre-common/scripts/wait-for-rabbitmq.sh", "rabbitmqctl add_user test test", "rabbitmqctl set_user_tags test administrator", "rabbitmqctl add_vhost test", "rabbitmqctl set_permissions -p /test test \".*\" \".*\" \".*\"" ]