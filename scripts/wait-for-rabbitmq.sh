#!/bin/sh

set -e

host="$1"
shift
cmd="$@"

until rabbitmq-server; do
  >&2 echo "RabbitMQ is unavailable - sleeping"
  sleep 1
done

>&2 echo "RabbitMQ is up - executing command"
exec $cmd