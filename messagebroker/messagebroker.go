package messagebroker

import (
	"fmt"
	"time"

	"github.com/streadway/amqp"
)

// MessageBroker contains the AMQP Connection, Channel, Queue, Publish and Consume needed
type MessageBroker struct {
	conn *amqp.Connection
	chnl *amqp.Channel
	qn   string
}

// MessageProcessor will process message received in Consume.
type MessageProcessor interface {
	ProcessRequest([]byte)
}

// New Message Broker
func New(qn string) *MessageBroker {
	mb := &MessageBroker{qn: qn}

	for i := 0; i < 5; i++ {
		conn, err := amqp.Dial("amqp://test:test@rabbitmq:5672")

		if err != nil {
			fmt.Printf("RabbitMQ connection failed: %v\n", err)
			time.Sleep(5 * time.Second)
		} else {
			mb.conn = conn
			ch, err := conn.Channel()

			if err != nil {
				fmt.Printf("RabbitMQ Channel failed to create: %v\n", err)
			}

			// This tells RabbitMQ not to give more than one message to a worker at a time.
			// Or, in other words, don't dispatch a new message to a worker until it has processed and acknowledged the previous one.
			// Instead, it will dispatch it to the next worker that is not still busy.
			ch.Qos(
				1,     // prefetch count
				0,     // prefetch size
				false, // global
			)

			_, qerr := ch.QueueDeclare(
				qn,    // name
				true,  // durable
				false, // delete when unused
				false, // exclusive
				false, // no-wait
				nil,   // arguments
			)

			if qerr != nil {
				fmt.Printf("RabbitMQ Queue failed to create: %v\n", qerr)
			}

			mb.chnl = ch
			break
		}
	}

	return mb
}

// Publish content to queue
func (mb *MessageBroker) Publish(b []byte) {
	fmt.Println("Publishing...")
	err := mb.chnl.Publish(
		"",    // exchange
		mb.qn, // routing key
		false, // mandatory
		false, // immediate
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "text/plain",
			Body:         b,
		})
	if err != nil {
		panic(err)
	}
}

// Consume content from task queue
func (mb *MessageBroker) Consume(mp MessageProcessor) {
	fmt.Println("Consuming....")
	msgs, err := mb.chnl.Consume(
		mb.qn, // queue
		"",    // consumer
		true,  // auto-ack
		false, // exclusive
		false, // no-local
		false, // no-wait
		nil,   // args
	)

	if err != nil {
		panic(err)
	}

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			mp.ProcessRequest(d.Body)
		}
	}()

	<-forever
}

// Close connection to Rabbit MQ
func (mb *MessageBroker) Close() {
	defer mb.conn.Close()
	defer mb.chnl.Close()
}
